\documentclass[a4paper,11pt]{article}

\usepackage[utf8x]{inputenc}
%\usepackage[czech]{babel}	%pro kompilaci LATEXem
%\usepackage{czech}		%pro kompilaci CSLATEXem
\usepackage[T1]{fontenc}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage[usenames]{color}
\usepackage{hyperref}
\usepackage{url}
\usepackage{tikz}
\usetikzlibrary{arrows}
\usetikzlibrary{backgrounds}
\usetikzlibrary{decorations.pathmorphing}
\usepackage{lettrine}
\usepackage{titlesec}
\usepackage{fancyhdr}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{listings}
\usepackage{caption}
\usepackage{subcaption}


\usepackage{siunitx}
\usepackage[europeanresistors,americanvoltages]{circuitikz}
\usepackage{mgbondgraph}



%========== okraje stranky ====================================

\oddsidemargin 0.5cm 
\evensidemargin 0.5cm

\topmargin 3pt
\headheight 15pt
\headsep 20pt

\textwidth 13cm

\marginparwidth 3cm 
\marginparsep 0.5cm 

\textheight 21cm
\footskip 2cm
 
%============== nove prikazy a prostredi ============================

\newtheorem{example}{Example}[section]

%======================================================

\definecolor{cvutdarkblue}{RGB}{0,99,168}
\definecolor{cvutlighterblue}{RGB}{112,153,204}
\definecolor{cvutlightestblue}{RGB}{217,227,245}
\definecolor{light-gray}{gray}{0.75}

\lstset{language=Matlab, basicstyle=\tiny, keywordstyle=\color{blue}, backgroundcolor=\color{light-gray}}

%============== zahlavi a zapati ======================

\pagestyle{fancy}

\fancyhead{}
\rhead{\bfseries Modeling based on Lagrange's equations}

\fancyfoot{}
\rfoot{\thepage}
\lfoot{Lecture 7 on Modeling and Simulation of Dynamic Systems}

\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

%\titleformat*{\section}{\fontfamily{pag}\fontsize{14pt}{16pt}\selectfont}
%============== opening ================================

\begin{document}

\thispagestyle{empty}

\begin{tikzpicture}[remember picture,overlay]

\coordinate [below=2.5cm] (midpoint) at (current page.north);

\node [name=colourbar,
draw=cvutlighterblue,
anchor=base,
fill=cvutlighterblue,
text = white,
minimum width=\paperwidth,
minimum height=1cm] at (midpoint) {};

% Define the point where the logo will go
\coordinate [right=20cm] (number) at (colourbar.west);

% Set coordinate system origin
\begin{scope}[shift=(number)]
% Draw the outline
\filldraw [cvutdarkblue] (1.3,0.5) -- ++(-2,0) -- ++(-0.8,-1) -- ++(2.8,0) --cycle;
% Include the logo
\node [text=white] {\Huge{\textbf{7}}};
\end{scope}

\end{tikzpicture}

%\vspace{-0.8cm}

\noindent
{\fontfamily{pag}\fontsize{31pt}{37pt}\selectfont  Modeling electrical and electromechanical systems using Lagrange's equation}\\
\vspace{0.8cm}
{\fontfamily{pag}\fontsize{12pt}{12pt}\selectfont  Unified energy-based modeling}\\
\vspace{0.0cm}
\begin{flushright}{\fontfamily{pag}\fontsize{12pt}{14pt}\fontshape{it}\selectfont  Zden\v ek Hur\' ak}\\\today\end{flushright}
\vspace{0.8cm}

\lettrine{M}{o}deling a dynamical system by evaluating its total kinetic (co)energy and total potential energy and substituting these into the Lagrange's equation is now a well established methodology. The method is covered in literally every textbook on classical/analytical/theoretical dynamics. However, these textbooks and, similarly, most introductory physics courses, favor examples in mechanical domain. The versatility of the Lagrange's methodology stemming from validity of energy across physical domains need not be perfectly obvious then. We are going to break it in this lecture by learning how to find a model of an electrical circuit following the Lagrange's approach. While this may seem redundant for electronic circuits because more taylored modeling methodologies exist, the apparent advantage is that we are going to use identical concepts that we have already exercised in the mechanical domain. This will give us a unified modeling framework for multidomain system such as electromechanical systems.   

\section{Modeling electrical circuits using Lagrange's equation}

\subsection{Charge formalism}
When looking for a candidate for the \textit{generalized variable}, our adherence to the notation that we established at the beginning of the course now pays back. One good candidate for playing the role of a generalized coordinate (or generalized displacement) labelled as $q$ is the electric charge (and the generalized velocity $\dot q$ is then the electric current $i=\mathrm{d}q/\mathrm{d}t$). 

But how many charges (currents) do we need to determine in the circuit in order to describe it fully? In other words, what is the number of degrees of freedom? 

Consider an example circuit below. A resistor, a capacitor and an inductor are interconnected in series together with a voltage source.

\begin{figure}[ht]
 \centering
\begin{tikzpicture}
\begin{circuitikz}
\draw (0,3)
to [european voltage source,v_=$u_0$,i<=$i_0$] (0,0); 
\draw (0,3)
to [R,l=$R$,*-*,i=$i_\mathrm{R}$, v_>=$v_\mathrm{R}$] (3,3)
to [L,l=$L$,-*,i=$i_\mathrm{L}$, v_>=$v_\mathrm{L}$] (6,3)
to [C,l=$C$,-,i=$i_\mathrm{C}$, v_>=$v_\mathrm{C}$] (9,3)
|- (0,0) 
{[anchor=south	] (0,3) node {	\color{red}1} (3,3) node {\color{red}2} (6,3) node {\color{red}3}};
\end{circuitikz}
\end{tikzpicture}
\caption{Circuit diagram of an electrical RLC circuit. Three nodes are labelled (the ground node not highlighted).}
\label{fig: Series RLC circuit}
\end{figure}

Thinking in terms of charges is perhaps not very intuitive, let's focus on derivatives of electric charges---electric currents as generalized velocities. Since there are four components here, hence four branches, we can label four currents. However, it is obvious that out of these four currents, three are dependent. In other words, there is just one independent current, hence charge. Let's label it just $q$. 

A general guidance for determining the number of degrees of freedom is given by the formula
\begin{equation}
 n = B-N+1,
\end{equation}
where $B$ is the total number of branches (a branch is a connection between two nodes, each branch has a single component) and $N$ is the number of nodes. The formula comes from the fact that there are $B$ branch currents and these have to satisfy $N-1$ Kirchhoff's current law conditions corresponding to $N-1$ nodes (the reference node is not considered). Hence the vector of branch currents lives in the $B-(N-1)$ subspace.

In our case, substituting into the formula gives $n=4-4+1=1$, which confirms our immediate conclusion that there is just one independent current, hence charge.

Now, our next task is to find the total kinetic coenergy and potential energy as functions of $q$ and $\dot q = i$. Note that in electrical circuits we prefer a different terminology: we classify the energies and coenergies as \textit{magnetic} and \textit{electric} rather than kinetic and potential. Therefore we write
\begin{equation}
\mathcal{W}_\mathrm{m}^*(\dot q) = \frac{1}{2}L\dot{q}^2
\end{equation}
and
\begin{equation}
\mathcal{W}_\mathrm{e}(q) = \frac{1}{2}\frac{1}{C}q^2.
\end{equation}

Lagrange function defined as
\begin{equation}
 \mathcal{L}(q,\dot{q}) = \mathcal{W}_\mathrm{m}^*(\dot q) - \mathcal{W}_\mathrm{e}(q)
\end{equation}
is then
\begin{equation}
 \mathcal{L}(q,\dot{q}) = \frac{1}{2}L\dot{q}^2 - \frac{1}{2}\frac{1}{C}q^2.
\end{equation}

Since the circuit also contains a dissipative element, its \textit{content} function (also Rayleigh dissipation function) is
\begin{equation}
 \mathcal{D}(\dot q) = \frac{1}{2}R\dot{q}^2.
\end{equation}

The nonconservative force $E_0$ (other than the dissipation) is just the single voltage $u_0$ of the generator.

Lagrange's equation defined as
\begin{equation}
 \frac{\mathrm{d}}{\mathrm{d}t}\frac{\partial}{\partial \dot{q}}L(q,\dot{q}) - \frac{\partial}{\partial q}L(q,\dot{q}) = -\frac{\partial}{\partial \dot q}\mathcal{D}(\dot q) + E_0
\end{equation}
is then
\begin{equation}
\frac{1}{C}q(t) + R \dot{q}(t) + L\ddot{q}(t) = u_0(t).
\end{equation}

This is our desired model. It interpretation is obvious. On the left hand side there is a sum of voltages on the three passive elements. Voilà, Kirchhoff's voltage law.

\subsection{Flux linkage formalism}
Although the choice of the charge as the generalized coordinate seemed natural (also thanks to the symbol $q$), there is another choice. It is related to the fact that it is not only the above defined Lagrangian that satisfies the Lagrange's equation but there is also something called co-Lagrangian defined as
\begin{equation}
 \mathcal{L}^*(\lambda,\dot\lambda) = \mathcal{W}_\mathrm{e}^*(\dot\lambda) - \mathcal{W}_\mathrm{m}(\lambda)
\end{equation}
that also satisfies the Lagrange's equation, namely
\begin{equation}
 \frac{\mathrm{d}}{\mathrm{d}t}\frac{\partial}{\partial \dot{\lambda}}\mathcal{L}^*(\lambda,\dot\lambda) - \frac{\partial}{\partial \lambda}\mathcal{L}^*(\lambda,\dot\lambda) = -\frac{\partial}{\partial \dot\lambda}\mathcal{D}^*(\dot \lambda) + I_0.
\end{equation}

Obviously, the promised new candidate for a generalized coordinate is \textit{flux linkage} $\lambda$. Its time derivative is voltage, that is, $\dot \lambda = u$ , and it may be a bit more comfortable for most of us to think in terms of voltages. How many voltages do we need to characterize the circuit? The number of nongrounded nodes gives an initial guess

\begin{equation}
 m = N-1.
\end{equation}

In our case $m = 4-1=3$. But note that the voltage on the first node is directly given by the generator. Hence it should not be regarded as a free coordinate. Remember that a generalized coordinate is something that will only be determined by solving the differential equation. Here its time evolution will be directly determined by the voltage source. Thus we need to consider two voltages, hence flux linkages as generalized coordinates: $\lambda_2$ and $\lambda_3$.

The electric (potential) coenergy is
\begin{equation}
 \mathcal{W}_\mathrm{e}^*(\dot\lambda) =\frac{1}{2}C\dot\lambda_3^2,
\end{equation}
the magnetic (kinetic) energy is
\begin{equation}
 \mathcal{W}_\mathrm{m}(\lambda) =\frac{1}{2}\frac{1}{L}(\lambda_2-\lambda_3)^2.
\end{equation}
The \textit{co-content} function is
\begin{equation}
 \mathcal{D}^*(\dot\lambda) =\frac{1}{2}\frac{1}{R}(\dot\lambda_1-\dot\lambda_2)^2.
\end{equation}

There is no source of current, hence there will be no term on the right-hand side of the two equations after moving the terms corresponding to dissipation to the left. Indeed, there will be two Lagrange's equations here
\begin{align}
 \frac{\mathrm{d}}{\mathrm{d}t}\frac{\partial}{\partial \dot{\lambda_2}}\mathcal{L}^* - \frac{\partial}{\partial \lambda_2}\mathcal{L}^* + \frac{\partial}{\partial \dot \lambda_2}\mathcal{D}^* &= 0\nonumber\\
 \frac{\mathrm{d}}{\mathrm{d}t}\frac{\partial}{\partial \dot{\lambda}_3}\mathcal{L}^* - \frac{\partial}{\partial \lambda_3}\mathcal{L}^* + \frac{\partial}{\partial \dot \lambda_3}\mathcal{D}^* &= 0,
\end{align}
which after substition take the form of
\begin{align}
 \frac{\lambda_2-\lambda_3}{L} - \frac{\dot\lambda_1-\dot\lambda_2}{R} &=0\label{eq:co-Lagrange's eq 1}\\
 -\frac{\lambda_2-\lambda_3}{L}+ C\ddot\lambda_3 &= 0\label{eq:co-Lagrange's eq 2},
\end{align}
where we should keep in mind that $\lambda_1$ is an input to the system ($u_0=\dot\lambda_1$).

The interpretation of these two equations is that: 1. the current going through the inductor is equal to the current through the resistor and 2. the current going through the inductor is equal to the current through the capacitor.

A disconcerting feature of the above model is that we have two variables in the equations. One appears together with its first derivative while the other also appears with its second derivative. This suggests that the corresponding state-space model would be of order 3. This does not agree with our observation that we only have two accumulators of energy (L and C elements). Let's see if we can massage the equations to reveal this information. We first sum the two equations to obtain
\begin{equation}
 -\frac{\dot\lambda_1-\dot\lambda_2}{R} + C\ddot\lambda_3 = 0,
\end{equation}
whose interpretation is, no surprise, that the current through a resistor is equal to the current through the capacitor. Notice that the equation contains only derivatives (first and second) of the variables. Therefore, we can integrate this equation to effectively reduce its order
\begin{equation}
 -\frac{\lambda_1-\lambda_2}{R} + C\dot\lambda_3 = 0.
\end{equation}

Together with (\ref{eq:co-Lagrange's eq 1}) this gives a new set of equations
\begin{align}
 \frac{\lambda_2-\lambda_3}{L} - \frac{\dot\lambda_1-\dot\lambda_2}{R} &=0\\
-\frac{\lambda_1-\lambda_2}{R} + C\dot\lambda_3 &= 0.
\end{align}

Thus, we have a set of two first-order equations, hence a system of order two, which agrees with the model obtained using the charge formalism.

Transforming these into a state-space model is an easy exercise left to you. 

% \begin{figure}[ht]
%  \centering
% \begin{tikzpicture}
% \begin{circuitikz}
% \draw (0,0)
% to [V,v=$u_0$] (0,6); 
% \draw (0,6)--(3,6);
% \draw (3,6)
% to [R,l=$R_1$,*-*,i=$i_1$] (3,3)
% to [R,l=$R_2$,-*,i=$i_2$] (6,3)
% |- (0,0); 
% \draw (3,6) 
% -- (6,6)
% to [L,l=$L$,i=$i_L$] (6,3);
% \draw (3,3)
% to [C=$C$,-*,i=$i_C$] (3,0);
% \end{circuitikz}
% \end{tikzpicture}
% \caption{Circuit diagram of an electrical RLC circuit.}
% \label{fig: RLC circuit}
% \end{figure}

\section{Modeling electromechanical systems using Lagrange's equation}

Not much new theory needed. Instead, we will clarify the methodology by a few examples. They are mostly from \cite{preumont_mechatronics_2006} as most of the material in this lecture.

\subsection{Electromagnetic plunger}



\subsection{Electromagnetic loudspeaker}

\subsection{Proof-mass actuator}

\subsection{Electrodynamic isolator}

\subsection{Sky-hook damper}

\section{Modeling transducers using energy}

\section{Literature}
The energy-based Lagrange's modeling has been described in gazzilions of wonderfull (and less wonderful) texts. Among our most favourite classics are \cite{lanczos_variational_1970} and \cite{greenwood_classical_1997} and the newer \cite{greenwood_advanced_2006}, \cite{jose_classical_1998} or even the student-friendly \cite{hamill_students_2013}. The trouble with all these texts is that they are exclusively driven by the needs of mechanical engineers. The same argument holds for the popular textbooks on robotics that also contain chapters on modeling, such as \cite{spong_robot_2005} or \cite{sicilliano_robotics_2009}. 

Luckily, there are a few texts that emphasize the multidomain approach. A recommendable text is \cite{preumont_mechatronics_2006}. In fact, I used it to prepare this lecture. 

I also recommend the tutorial paper \cite{jeltsema_multidomain_2009} which describes the duality in Lagrange's modeling with quite some rigor. There are a few other recent texts such as \cite{fabien_analytical_2008} or \cite{jones_electromechanics_2013} but, personally, nothing can beat the wonderful three-volume classic \cite{woodson_electromechanical_1968}. The more so that it is freely (and legally) downloadable at \url{http://ocw.mit.edu/resources/res-6-003-electromechanical-dynamics-spring-2009/front-end-matter/}. 

\bibliographystyle{plain}
\bibliography{../msd}
 

\end{document}
